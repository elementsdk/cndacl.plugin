<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 05/04/14
 * Time: 13:50
 */

namespace CndAcl\Controller\Component\Acl;

use Cake\Controller\Component\Acl\AclInterface;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\PermissionsTable;

/**
 * Class CndDbAcl
 *
 * @package CndAcl\Controller\Component\Acl
 */
class CndDbAcl implements AclInterface {

	/**
	 * @var PermissionsTable
	 */
	private $Permissions;

	/**
	 * Empty method to be overridden in subclasses
	 *
	 * @param string $aro ARO The requesting object identifier.
	 * @param string $aco ACO The controlled object identifier.
	 * @param string $action Action (defaults to *)
	 * @return boolean Success
	 */
	public function check($aro, $aco, $action = "*") {
		return $this->Permissions->check($aro, $aco, $action);
	}

	/**
	 * Allow methods are used to grant an ARO access to an ACO.
	 *
	 * @param string $aro ARO The requesting object identifier.
	 * @param string $aco ACO The controlled object identifier.
	 * @param string $action Action (defaults to *)
	 * @return boolean Success
	 */
	public function allow($aro, $aco, $action = "*") {
		return $this->Permissions->allow($aro, $aco, $action);
	}

	/**
	 * Deny methods are used to remove permission from an ARO to access an ACO.
	 *
	 * @param string $aro ARO The requesting object identifier.
	 * @param string $aco ACO The controlled object identifier.
	 * @param string $action Action (defaults to *)
	 * @return boolean Success
	 */
	public function deny($aro, $aco, $action = "*") {
		return $this->Permissions->allow($aro, $aco, $action, -1);
	}

	/**
	 * Inherit methods modify the permission for an ARO to be that of its parent object.
	 *
	 * @param string $aro ARO The requesting object identifier.
	 * @param string $aco ACO The controlled object identifier.
	 * @param string $action Action (defaults to *)
	 * @return boolean Success
	 */
	public function inherit($aro, $aco, $action = "*") {
		return $this->allow($aro, $aco, $action, 0);
	}

	/**
	 * Initialization method for the Acl implementation
	 *
	 * @param Component $component
	 * @return void
	 */
	public function initialize(Component $component) {
		$this->Permissions = TableRegistry::get('CndAcl.Permissions');
	}

}