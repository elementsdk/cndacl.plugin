<?php
namespace CndAcl\Controller;

use CndAcl\Controller\AppController;
use Cake\Event\Event;

/**
 * AclUsers Controller
 *
 * @property CndAcl\Model\Table\AclUsersTable $AclUsers
 */
class AclUsersController extends AppController {


	/**
	 * @param Event $event
	 */
	public function beforeFilter(Event $event) {

		parent::beforeFilter($event);

		$this->Auth->allow(['login']);
	}


	/**
	 * Login Method
	 */
	public function login() {

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Session->setFlash(__('Invalid username or password, try again'));
		}
	}


	/**
	 * Index method
	 *
	 * @return void
	 */
	public function index() {
		$this->set('aclUsers', $this->paginate($this->AclUsers));
	}

	/**
	 * View method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$aclUser = $this->AclUsers->get($id, [
			'contain' => []]);
		$this->set('aclUser', $aclUser);
	}

	/**
	 * Add method
	 *
	 * @return void
	 */
	public function add() {
		$aclUser = $this->AclUsers->newEntity($this->request->data);
		if ($this->request->is('post')) {
			if ($this->AclUsers->save($aclUser)) {
				$this->Session->setFlash(__('The acl user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('The acl user could not be saved. Please, try again.'));
			}
		}

		$aclGroups = $this->AclUsers->AclGroups->find('list')->toArray();
		debug($aclGroups);
		$this->set(compact('aclUser', 'aclGroups'));
	}

	/**
	 * Edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$aclUser = $this->AclUsers->get($id, [
			'contain' => []]);
		if ($this->request->is([
			'post',
			'put'])
		) {
			$aclUser = $this->AclUsers->patchEntity($aclUser, $this->request->data);
			if ($this->AclUsers->save($aclUser)) {
				$this->Session->setFlash(__('The acl user has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('The acl user could not be saved. Please, try again.'));
			}
		}

		$aclGroups = $this->AclUsers->AclGroups->find('list')->all();
		debug($aclGroups);
		$this->set(compact('aclUser', 'aclGroups'));
	}

	/**
	 * Delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$aclUser = $this->AclUsers->get($id);
		$this->request->allowMethod('post', 'delete');
		if ($this->AclUsers->delete($aclUser)) {
			$this->Session->setFlash(__('The acl user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The acl user could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
