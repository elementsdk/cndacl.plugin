<?php
namespace CndAcl\Controller;

use CndAcl\Controller\AppController;

/**
 * AclGroups Controller
 *
 * @property CndAcl\Model\Table\AclGroupsTable $AclGroups
 */
class AclGroupsController extends AppController {

/**
 * Index method
 *
 * @return void
 */
	public function index() {
		$this->set('aclGroups', $this->paginate($this->AclGroups));
	}

/**
 * View method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$aclGroup = $this->AclGroups->get($id, [
			'contain' => []
		]);
		$this->set('aclGroup', $aclGroup);
	}

/**
 * Add method
 *
 * @return void
 */
	public function add() {
		$aclGroup = $this->AclGroups->newEntity($this->request->data);
		if ($this->request->is('post')) {
			if ($this->AclGroups->save($aclGroup)) {
				$this->Session->setFlash(__('The acl group has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('The acl group could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('aclGroup'));
	}

/**
 * Edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$aclGroup = $this->AclGroups->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['post', 'put'])) {
			$aclGroup = $this->AclGroups->patchEntity($aclGroup, $this->request->data);
			if ($this->AclGroups->save($aclGroup)) {
				$this->Session->setFlash(__('The acl group has been saved.'));
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('The acl group could not be saved. Please, try again.'));
			}
		}
		$this->set(compact('aclGroup'));
	}

/**
 * Delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$aclGroup = $this->AclGroups->get($id);
		$this->request->allowMethod('post', 'delete');
		if ($this->AclGroups->delete($aclGroup)) {
			$this->Session->setFlash(__('The acl group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The acl group could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
	}
}
