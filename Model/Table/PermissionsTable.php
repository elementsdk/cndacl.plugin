<?php
namespace CndAcl\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\ArosTable;
use Cake\Utility\Hash;

/**
 * Class PermissionsTable
 *
 * @package CndAcl\Model\Table
 */
class PermissionsTable extends Table {

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config) {

		$this->table('aros_acos');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->belongsTo('Aros', [
			'className'  => 'CndAcl.Aros',
			'foreignKey' => 'aro_id']);

		$this->belongsTo('Acos', [
			'className'  => 'CndAcl.Acos',
			'foreignKey' => 'aco_id']);

	}


	/**
	 * @param array $aro
	 * @param array $aco
	 * @param string $action
	 */
	public function allow($aro = [], $aco = [], $actions = '*', $value = 1) {

		$perms = $this->getAclLink($aro, $aco);
		if (!$perms) {
			throw new \RuntimeException(__d('cake_dev', '%s - Invalid node', 'DbAcl::allow()'));
		}
		$permKeys = $this->getAcoKeys($this->schema()->columns());

		if (isset($perms[0])) {
			$save = $perms[0][$this->alias()];
		}

		if ($actions === '*') {
			$save = array_combine($permKeys, array_pad(array(), count($permKeys), $value));
		} else {
			if (!is_array($actions)) {
				$actions = array('_' . $actions);
			}
			foreach ($actions as $action) {
				if ($action{0} !== '_') {
					$action = '_' . $action;
				}
				if (!in_array($action, $permKeys, true)) {
					throw new \RuntimeException(__d('cake_dev', 'Invalid permission key "%s"', $action));
				}
				$save[$action] = $value;
			}
		}

		list($save['aro_id'], $save['aco_id']) = array(
			$perms['aro']->first()->id,
			$perms['aco']->first()->id);

		if ($perms['link'] && !empty($perms['link'])) {
			$save['id'] = $perms['link']->id;
		} else {
			unset($save['id']);
		}

		$permission = $this->newEntity($save);
		$this->save($permission);

	}


	/**
	 * Checks if the given $aro has access to action $action in $aco
	 *
	 * @param string $aro ARO The requesting object identifier.
	 * @param string $aco ACO The controlled object identifier.
	 * @param string $action Action (defaults to *)
	 * @return boolean Success (true if ARO has access to action in ACO, false otherwise)
	 */
	public function check($aro, $aco, $action = '*') {

		if (!$aro || !$aco) {
			return false;
		}

		$permKeys = $this->getAcoKeys($this->schema()->columns());
		$aroPath  = $this->Aros->node($aro);
		$acoPath  = $this->Acos->node($aco);

		$inherited = array();

		$acoIDs = $acoPath->all()
			->extract('id')
			->reduce(function ($output, $value) {
				if (!in_array($value, $output)) {
					$output[] = $value;
				}
				return $output;
			}, []);


		if (!$aroPath) {
			throw new \RuntimeException('ARO Node Failed');
			return false;
		}

		if (!$acoIDs) {
			throw new \RuntimeException('ACO Nodes Failed');
			return false;
		}

		if ($action !== '*' && !in_array('_' . $action, $permKeys)) {
			throw new \RuntimeException('Permission not found');
			return false;
		}

		$aros = $aroPath->find('all');

		foreach ($aros as $aro) {

			$permAlias = $this->alias();

			$perms = $this->find('all', [
				'order'   => [$this->Acos->alias() . '.lft' => 'desc'],
				'contain' => ['Acos']])
				->where([
					"aco_id"              => $acoIDs,
					"{$permAlias}.aro_id" => $aro->id], ['aco_id' => 'integer[]']);

			foreach ($perms as $perm) {
				if ($action === '*') {

					foreach ($permKeys as $key) {
						if ($perm->{$key} == -1) {
							return false;
						} elseif ($perm->{$key} == 1) {
							$inherited[$key] = 1;

						}
					}
					if (count($inherited) === count($permKeys)) {
						return true;
					}

				} else {
					switch ($perm->{"_$action"}) {
						case -1:
							return false;
						case 0:
							continue;
						case 1:
							return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Get an array of access-control links between the given Aro and Aco
	 *
	 * @param string $aro ARO The requesting object identifier.
	 * @param string $aco ACO The controlled object identifier.
	 * @return array Indexed array with: 'aro', 'aco' and 'link'
	 */
	public function getAclLink($aro, $aco) {

		$aro = $this->Aros->node($aro);
		$aco = $this->Acos->node($aco);

		if (empty($aro) || empty($aco)) {
			return false;
		}

		//debug($aro->first()->id);
		$aro_id = $aro->first()->id;
		$aco_id = $aco->first()->id;

		//die();
		return array(
			'aro'  => $aro,
			'aco'  => $aco,
			'link' => $this->find('all', array(
				'conditions' => array(
					$this->alias() . '.aro_id' => $aro_id,
					$this->alias() . '.aco_id' => $aco_id)))
				->first());
	}

	/**
	 * Get the crud type keys
	 *
	 * @param array $keys Permission schema
	 * @return array permission keys
	 */
	public function getAcoKeys($keys) {
		$newKeys = array();
		foreach ($keys as $key) {
			if (!in_array($key, array(
				'id',
				'aro_id',
				'aco_id'))
			) {
				$newKeys[] = $key;
			}
		}
		return $newKeys;
	}


}
