<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 19/04/14
 * Time: 19:42
 */

namespace CndAcl\Model\Table;

use Cake\ORM\Table;

class AclArticlesTable extends Table {

	/**
	 * @param array $config
	 */
	public function initialize(array $config) {
		$this->table('articles');
	}

}
