<?php
namespace CndAcl\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Acos Model
 */
class AcosTable extends Table {

	use AclTrait;

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config) {

		$this->table('acos');
		$this->displayField('id');
		$this->primaryKey('id');
		$this->addBehavior('Tree');

//		$this->belongsTo('ParentAcos', [
//			'foreignKey' => 'parent_id',]);
//
//		$this->hasMany('ChildAcos', [
//			'foreignKey' => 'parent_id',]);

		$this->belongsToMany('Aros', [
			'foreignKey' => 'aco_id',
			'targetForeignKey' => 'aro_id',
			'joinTable' => 'aros_acos',]);
	}

}
