<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 19/04/14
 * Time: 19:42
 */

namespace CndAcl\Model\Table;

use Cake\ORM\Table;

class AclGroupsTable extends Table {

	/**
	 * @param array $config
	 */
	public function initialize(array $config) {

		$this->table('acl_groups');
		$this->hasMany('CndAcl.AclUsers');
		$this->addBehavior('Timestamp');
		$this->addBehavior('CndAcl.CndAcl', $options = ['type' => 'requester']);

	}

}
