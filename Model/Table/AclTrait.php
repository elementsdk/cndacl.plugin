<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 13/04/14
 * Time: 13:15
 */

namespace CndAcl\Model\Table;

use Cake\ORM\Query;

trait AclTrait {


	/**
	 * @param array|string $ref
	 * @return Query
	 */
	public function node($ref = array()) {

		$type = $this->alias();

		if (is_array($ref) && count($ref) === 1) {
			$completeModel = array_keys($ref)[0];

			$model = pluginSplit($completeModel)[1];
			$ref = [
				$type . '.model' => $model,
				$type . '.foreign_key' => $ref[$completeModel]['id']];

		}

		if (is_string($ref)) {

			$path = explode('/', $ref);
			$start = $path[0];
			unset($path[0]);

			$query = $this->find('all')

				->select([
					'id' => "{$type}.id",
					'lft' => "{$type}.lft",
					'rght' => "{$type}.rght",
					'foreign_key' => "{$type}.foreign_key",
					'model' => "{$type}.model",
					'parent_id' => "{$type}.parent_id",
					'alias' => "{$type}.alias"])

				->where([
					"{$type}.lft" . ' <= ' . "c0.lft",
					"{$type}.rght" . ' >= ' . "c0.rght"])
				->order(["c0.lft" => 'DESC'])
				->join([
					'c0' => [
						'table' => $this->table(),
						'type' => 'INNER',
						'conditions' => ["c0.alias" => "$start"]]]);

			foreach ($path as $i => $alias) {

				$j = $i - 1;

				$query
					->join([
						"c$i" => [
							'table' => $this->table(),
							'type' => 'INNER',
							'conditions' => [
								"c{$i}.lft" . ' > ' . "c{$j}.lft",
								"c{$i}.rght" . ' < ' . "c{$j}.rght",
								"c{$i}.alias" . ' = ' . '"' . $alias . '"',
								"c{$j}.id" . ' = ' . "c{$i}.parent_id"]]])
					->where([
						"{$type}.lft" . ' <= ' . "c0.lft" . ' AND ' . "{$type}.rght" . ' >= ' . "c0.rght" . " OR " . "{$type}.lft" . ' <= ' . "c{$i}.lft" . ' AND ' . "{$type}.rght" . ' >= ' . "c{$i}.rght"], [], true);


			}

		} else {


			$query = $this->find('all')
				->select([
					'id' => 'c.id',
					'lft' => 'c.lft',
					'rght' => 'c.rght',
					'foreign_key' => 'c.foreign_key',
					'model' => 'c.model',
					'parent_id' => 'c.parent_id',
					'alias' => 'c.alias',])
				->where($ref)
				->order(["c.lft" => 'DESC'])
				->join([
					'c' => [
						'table' => $this->table(),
						'type' => 'INNER',
						'conditions' => [
							"c.lft" . ' <= ' . "{$type}.lft",
							"c.rght" . ' >= ' . "{$type}.rght"]

					]]);
		}

		return $query;

	}

}