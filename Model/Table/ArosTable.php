<?php
namespace CndAcl\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Aros Model
 */
class ArosTable extends Table {

	use AclTrait;

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config) {
		$this->table('aros');
		$this->displayField('id');
		$this->primaryKey('id');
		$this->addBehavior('Tree');

		$this->belongsToMany('Acos', [
			'foreignKey' => 'aro_id',
			'targetForeignKey' => 'aco_id',
			'joinTable' => 'aros_acos',]);
	}

}
