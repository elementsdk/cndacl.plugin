<?php

namespace CndAcl\Model\Behavior;

use Cake\Error\Exception;
use Cake\Event\Event;
use Cake\ORM\Association;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use CndAcl\Model\Behavior\Error\MissingParentNodeException;

/**
 * Class CndAclBehavior
 *
 * @package Cnd\Model\Behavior
 */
class CndAclBehavior extends Behavior {

	/**
	 * Maps ACL type options to ACL models
	 *
	 * @var array
	 */
	protected $_typeMaps = [
		'requester' => 'Aros',
		'controlled' => 'Acos',
		'both' => [
			'Aros',
			'Acos']];

	/**
	 * Keeping a reference to the table
	 *
	 * @var \Cake\ORM\Table
	 */
	protected $_table;


	/**
	 * @param Table $table
	 * @param array $config
	 */
	public function __construct(Table $table, array $config = []) {
		parent::__construct($table, $config);
		$this->_table = $table;
		$this->Acos = TableRegistry::get('CndAcl.Acos');
		$this->Aros = TableRegistry::get('CndAcl.Aros');
	}


	/**
	 * @param Event $event
	 * @param Entity $entity
	 */
	public function afterSave(Event $event, Entity $entity) {

		$types = $this->_typeMaps[$this->_config['type']];
		if (!is_array($types)) {
			$types = array($types);
		}

		if (!is_callable([
			$entity,
			'parentNode'])
		) {
			throw new MissingParentNodeException('Your Entity is missing a Entity::parentNode() method');
		}

		$parent = $entity->parentNode();
		foreach ($types as $type) {
			$parent_node = $this->node($parent, $type);
			$node = $this->node($entity, $type);
			$savenode = $this->{$type}->newEntity([
				'parent_id' => isset($parent_node->id) ? $parent_node->id : null,
				'foreign_key' => $entity->id,
				'model' => $entity->source(),
				'id' => isset($node->id) ? $node->id : null]);
			$this->{$type}->save($savenode);
		}
	}


	/**
	 * @param Event $event
	 * @param Entity $entity
	 */
	public function afterDelete(Event $event, Entity $entity) {

		$types = $this->_typeMaps[$this->_config['type']];
		if (!is_array($types)) {
			$types = array($types);
		}

		foreach ($types as $type) {
			$acl_node = $this->node($entity, $type);
			$this->{$type}->delete($acl_node->find('all')->first());
		}

	}

	/**
	 * Retrieves the Aro/Aco node for the entity
	 *
	 * @param Entity|bool $entity
	 * @param null $type
	 * @return Entity|bool
	 */
	public function node($entity, $type = null) {
		if ($entity) {
			$ref = array(
				$type . '.model' => $entity->source(),
				$type . '.foreign_key' => $entity->id);

			return $this->{$type}->node($ref);
		}

		return false;
	}

}
