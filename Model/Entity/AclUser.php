<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 19/04/14
 * Time: 19:47
 */

namespace CndAcl\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\AclArticlesTable;
use Cake\Controller\Component\Auth\BlowfishPasswordHasher;

class AclUser extends Entity {


	/**
	 * @return mixed
	 */
	public function parentNode() {

		$table  = TableRegistry::get('CndAcl.AclGroups');
		$result = $table->find('all')
			->where(['id' => $this->acl_group_id])
			->first();

		return $result;
	}


	/**
	 * @param $password
	 * @return mixed
	 */
	public function setPassword($password) {
		return (new BlowfishPasswordHasher)->hash($password);
	}

} 