<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 19/04/14
 * Time: 19:47
 */

namespace CndAcl\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\AclArticlesTable;

class AclGroup extends Entity {


	/**
	 * @return mixed
	 */
	public function parentNode() {
		return null;
	}

} 