<?php
namespace CndAcl\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArosAco Entity.
 */
class ArosAco extends Entity {

	/**
	 * Fields that can be mass assigned using newEntity() or patchEntity().
	 *
	 * @var array
	 */
	protected $_accessible = [
		'aro_id' => true,
		'aco_id' => true,
		'_create' => true,
		'_read' => true,
		'_update' => true,
		'_delete' => true,];


}
