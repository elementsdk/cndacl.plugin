<?php
namespace CndAcl\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AroFixture
 *
 */
class AroFixture extends TestFixture {

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = [
		'id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'autoIncrement' => true,
			'precision' => null],
		'parent_id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'model' => [
			'type' => 'string',
			'length' => 255,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'foreign_key' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'alias' => [
			'type' => 'string',
			'length' => 255,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'lft' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'rght' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'_indexes' => [
			'parent_id' => [
				'type' => 'index',
				'columns' => ['parent_id'],
				'length' => []],
			'foreign_key' => [
				'type' => 'index',
				'columns' => ['foreign_key'],
				'length' => []],
			'lft' => [
				'type' => 'index',
				'columns' => ['lft'],
				'length' => []],
			'model' => [
				'type' => 'index',
				'columns' => ['model'],
				'length' => []],
			'rght' => [
				'type' => 'index',
				'columns' => ['rght'],
				'length' => []]],
		'_constraints' => [
			'primary' => [
				'type' => 'primary',
				'columns' => ['id'],
				'length' => []]]];

	/**
	 * @var array
	 */
	public $records = [
		[
			'id' => 1,
			'parent_id' => null,
			'model' => 'AclGroups',
			'foreign_key' => 1,
			'lft' => 1,
			'rght' => 4],
		[
			'id' => 2,
			'parent_id' => 1,
			'model' => 'AclUsers',
			'foreign_key' => 1,
			'lft' => 2,
			'rght' => 3]
	];
	
}
