<?php
namespace CndAcl\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArosAcoFixture
 *
 */
class ArosAcoFixture extends TestFixture {

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = [
		'id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'autoIncrement' => true,
			'precision' => null],
		'aro_id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'aco_id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'_create' => [
			'type' => 'string',
			'length' => 2,
			'null' => false,
			'default' => '0',
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'_read' => [
			'type' => 'string',
			'length' => 2,
			'null' => false,
			'default' => '0',
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'_update' => [
			'type' => 'string',
			'length' => 2,
			'null' => false,
			'default' => '0',
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'_delete' => [
			'type' => 'string',
			'length' => 2,
			'null' => false,
			'default' => '0',
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'_constraints' => [
			'primary' => [
				'type' => 'primary',
				'columns' => ['id'],
				'length' => []],
			'ARO_ACO_KEY' => [
				'type' => 'unique',
				'columns' => [
					'aro_id',
					'aco_id'],
				'length' => []],],];


}
