<?php
namespace CndAcl\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcoFixture
 *
 */
class AclUserFixture extends TestFixture {

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = [
		'id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'autoIncrement' => true,
			'precision' => null],
		'acl_group_id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null],
		'username' => [
			'type' => 'string',
			'length' => 255,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'password' => [
			'type' => 'string',
			'length' => 255,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'_constraints' => [
			'primary' => [
				'type' => 'primary',
				'columns' => ['id'],
				'length' => []],],];

	public $records = [
		[
			'id' => 1,
			'acl_group_id' => 1,
			'username' => 'admin',
			'password' => 1234]];

}
