<?php
namespace CndAcl\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcoFixture
 *
 */
class AcoFixture extends TestFixture {

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = [
		'id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'autoIncrement' => true,
			'precision' => null],
		'parent_id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'model' => [
			'type' => 'string',
			'length' => 255,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'foreign_key' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'alias' => [
			'type' => 'string',
			'length' => 255,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'fixed' => null],
		'lft' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'rght' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'_indexes' => [
			'parent_id' => [
				'type' => 'index',
				'columns' => ['parent_id'],
				'length' => []],
			'foreign_key' => [
				'type' => 'index',
				'columns' => ['foreign_key'],
				'length' => []],
			'alias' => [
				'type' => 'index',
				'columns' => ['alias'],
				'length' => []],
			'model' => [
				'type' => 'index',
				'columns' => ['model'],
				'length' => []],
			'lft' => [
				'type' => 'index',
				'columns' => ['lft'],
				'length' => []],
			'rght' => [
				'type' => 'index',
				'columns' => ['rght'],
				'length' => []],],
		'_constraints' => [
			'primary' => [
				'type' => 'primary',
				'columns' => ['id'],
				'length' => []],],];


	public $records = [
		[
			'id' => 1,
			'parent_id' => null,
			'model' => 'Nodes',
			'foreign_key' => 1000,
			'lft' => 1,
			'rght' => 8],
		[
			'id' => 2,
			'parent_id' => 1,
			'model' => 'Nodes',
			'foreign_key' => 1001,
			'lft' => 2,
			'rght' => 5],

		[
			'id' => 3,
			'parent_id' => 2,
			'model' => 'Nodes',
			'foreign_key' => 1002,
			'lft' => 3,
			'rght' => 4],

		[
			'id' => 4,
			'parent_id' => 1,
			'model' => 'Nodes',
			'foreign_key' => 1003,
			'lft' => 6,
			'rght' => 7],
		[
			'id' => 5,
			'parent_id' => null,
			'model' => null,
			'foreign_key' => null,
			'alias' => 'controllers',
			'lft' => 9,
			'rght' => 14],

		[
			'id' => 6,
			'parent_id' => 5,
			'model' => null,
			'foreign_key' => null,
			'alias' => 'MyController',
			'lft' => 10,
			'rght' => 13],

		[
			'id' => 7,
			'parent_id' => 6,
			'model' => null,
			'foreign_key' => null,
			'alias' => 'myaction',
			'lft' => 11,
			'rght' => 12],


	];

}
