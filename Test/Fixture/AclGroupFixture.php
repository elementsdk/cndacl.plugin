<?php
namespace CndAcl\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AcoFixture
 *
 */
class AclGroupFixture extends TestFixture {

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = [
		'id' => [
			'type' => 'integer',
			'length' => 10,
			'unsigned' => false,
			'null' => false,
			'default' => null,
			'comment' => '',
			'autoIncrement' => true,
			'precision' => null],
		'name' => [
			'type' => 'string',
			'length' => 255,
			'unsigned' => false,
			'null' => true,
			'default' => null,
			'comment' => '',
			'precision' => null,
			'autoIncrement' => null],
		'_constraints' => [
			'primary' => [
				'type' => 'primary',
				'columns' => ['id'],
				'length' => []],],];


}
