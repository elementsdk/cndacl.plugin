<?php
namespace CndAcl\Test\TestCase\Console\Command;

use CndAcl\Console\Command\SetupShell;
use Cake\TestSuite\TestCase;

/**
 * CndAcl\Console\Command\SetupShell Test Case
 */
class SetupShellTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->io = $this->getMock('Cake\Console\ConsoleIo');
		$this->Setup = new SetupShell($this->io);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Setup);

		parent::tearDown();
	}

/**
 * testMain method
 *
 * @return void
 */
	public function testMain() {
		$this->markTestIncomplete('testMain not implemented.');
	}

}
