<?php

namespace CndAcl\Test\TestCase\Model\Behavior;

use Cake\ORM\TableRegistry;
use Cake\Test\TestCase\View\Form\EntityContextTest;
use CndAcl\Model\Table\AcosTable;
use Cake\TestSuite\TestCase;
use CndAcl\Model\Table\AclArticles;

class CndAclBehaviorTest extends TestCase {

	/**
	 * @var array
	 */
	public $fixtures = [
		'core.article',
		'plugin.cnd_acl.acl_user',
		'plugin.cnd_acl.acl_group',
		'plugin.cnd_acl.aco',
		'plugin.cnd_acl.aro',
		'plugin.cnd_acl.aros_aco',];


	/**
	 *
	 */
	public function tearDown() {
		parent::tearDown();
		TableRegistry::clear();
	}

	/**
	 * @throws \RuntimeException
	 */
	public function testCallbacks() {


		$articles = TableRegistry::get('CndAcl.AclArticles');
		$acos = TableRegistry::get('CndAcl.Acos');

		$articles->addBehavior('CndAcl.CndAcl',
			['type' => 'controlled']);

		$article = $articles->newEntity(['title' => 'my article']);
		$articles->save($article);

		$table = TableRegistry::get('CndAcl.AclGroups');
		$table->addBehavior('CndAcl.CndAcl',
			['type' => 'requester']);

		$group = $table->newEntity(['name' => 'admins']);
		$table->save($group);


		$table = TableRegistry::get('CndAcl.AclUsers');
		$table->addBehavior('CndAcl.CndAcl',
			['type' => 'requester']);

		$user = $table->newEntity(['username' => 'test@example.com', 'acl_group_id' => $group->id]);
		$table->save($user);

		$articles->delete($article);


	}

} 