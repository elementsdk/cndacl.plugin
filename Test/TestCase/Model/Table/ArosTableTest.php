<?php
namespace CndAcl\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\ArosTable;
use Cake\TestSuite\TestCase;

/**
 * CndAcl\Model\Table\ArosTable Test Case
 */
class ArosTableTest extends TestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = [
		'plugin.cnd_acl.aro',
		'plugin.cnd_acl.aco',
		'plugin.cnd_acl.aros_aco'];

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->markTestIncomplete('WIP');

		$config = TableRegistry::exists('Aros') ? [] : ['className' => 'CndAcl\Model\Table\ArosTable'];
		$this->Aros = TableRegistry::get('Aros', $config);
	}

	public function testDummy( ) {
		$this->markTestIncomplete('WIP');

	}


	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->Aros);

		parent::tearDown();
	}

}
