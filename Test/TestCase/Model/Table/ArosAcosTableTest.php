<?php
namespace CndAcl\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\ArosAcosTable;
use Cake\TestSuite\TestCase;

/**
 * CndAcl\Model\Table\ArosAcosTable Test Case
 */
class ArosAcosTableTest extends TestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = [
		'plugin.cnd_acl.aros_aco',
		'plugin.cnd_acl.aro',
		'plugin.cnd_acl.aco'];

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();
		$this->markTestIncomplete('WIP');

		$config = TableRegistry::exists('ArosAcos') ? [] : ['className' => 'CndAcl\Model\Table\ArosAcosTable'];
		$this->ArosAcos = TableRegistry::get('ArosAcos', $config);
	}

	public function testDummy( ) {
		$this->markTestIncomplete('WIP');

	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->ArosAcos);

		parent::tearDown();
	}

}
