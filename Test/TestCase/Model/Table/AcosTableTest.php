<?php
namespace CndAcl\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use CndAcl\Model\Table\AcosTable;
use Cake\TestSuite\TestCase;

/**
 * CndAcl\Model\Table\AcosTable Test Case
 */
class AcosTableTest extends TestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = [
		'plugin.cnd_acl.aco',
		'plugin.cnd_acl.aro',
		'plugin.cnd_acl.aros_aco'];

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp() {
		parent::setUp();

		$config = TableRegistry::exists('Acos') ? [] : ['className' => 'CndAcl\Model\Table\AcosTable'];
		$this->Acos = TableRegistry::get('Acos', $config);
	}

	public function testDummy( ) {
		$this->markTestIncomplete('WIP');

	}


	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown() {
		unset($this->Acos);

		parent::tearDown();
	}

}
