<?php

namespace Cake\Test\TestCase\Controller\Component\Acl;

use Cake\Controller\ComponentRegistry;
use Cake\Controller\Component\AclComponent;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;
use CndAcl\Controller\Component\Acl\CndDbAcl;


class CndDbAclTest extends TestCase {


	public $fixtures = [
		'core.article',
		'plugin.cnd_acl.acl_user',
		'plugin.cnd_acl.acl_group',
		'plugin.cnd_acl.aco',
		'plugin.cnd_acl.aro',
		'plugin.cnd_acl.aros_aco'];


	public function setUp() {
		parent::setUp();
		Configure::write('Acl.classname', 'CndAcl.CndDbAcl');
		$Collection = new ComponentRegistry();
		$this->CndDbAcl = new CndDbAcl();
		$this->Acl = new AclComponent($Collection);
	}


	public function testAllowDenyAndCheck() {

		//we never gave permissions to this one
		$this->assertFalse($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1000,
			'Acos.model' => 'Nodes']));

		//allow all or 1001
		$this->Acl->allow([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1001,
			'Acos.model' => 'Nodes']);

		$this->assertTrue($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1001,
			'Acos.model' => 'Nodes']));

		//1002 is a child of 1001, check that inheritance works
		$this->assertTrue($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1002,
			'Acos.model' => 'Nodes']));

		//explicitly allow delete
		$this->Acl->allow([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1002,
			'Acos.model' => 'Nodes'], ['delete']);

		$this->assertTrue($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1002,
			'Acos.model' => 'Nodes'], 'delete'));

		//now deny read
		$this->Acl->deny([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1002,
			'Acos.model' => 'Nodes'], ['read']);

		$this->assertFalse($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], [
			'Acos.foreign_key' => 1002,
			'Acos.model' => 'Nodes'], 'read'));


		$this->assertFalse($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], 'controllers/MyController/myaction'));


		$this->Acl->allow([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], 'controllers');

		$this->assertTrue($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], 'controllers/MyController/myaction'));

		$this->Acl->deny([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], 'controllers/MyController');

		$this->assertFalse($this->Acl->check([
			'Aros.foreign_key' => 1,
			'Aros.model' => 'AclUsers'], 'controllers/MyController/myaction'));


	}


	/**
	 * test that a deny rule wins over an equally specific allow rule
	 *
	 * @return void
	 */
	public function testDenyRuleIsStrongerThanAllowRule() {
	}
}

