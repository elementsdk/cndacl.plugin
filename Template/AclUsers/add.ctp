<div class="aclUsers form">
<?= $this->Form->create($aclUser); ?>
	<fieldset>
		<legend><?= __('Add Acl User'); ?></legend>
	<?php
		echo $this->Form->input('acl_group_id');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
	?>
	</fieldset>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('List Acl Users'), ['action' => 'index']); ?></li>
	</ul>
</div>
