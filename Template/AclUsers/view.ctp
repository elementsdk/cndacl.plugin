<div class="aclUsers view">
	<h2><?= __('Acl User'); ?></h2>
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd>
			<?= h($aclUser->id); ?>
			&nbsp;
		</dd>
		<dt><?= __('Acl Group Id'); ?></dt>
		<dd>
			<?= h($aclUser->acl_group_id); ?>
			&nbsp;
		</dd>
		<dt><?= __('Username'); ?></dt>
		<dd>
			<?= h($aclUser->username); ?>
			&nbsp;
		</dd>
		<dt><?= __('Password'); ?></dt>
		<dd>
			<?= h($aclUser->password); ?>
			&nbsp;
		</dd>
		<dt><?= __('Created'); ?></dt>
		<dd>
			<?= h($aclUser->created); ?>
			&nbsp;
		</dd>
		<dt><?= __('Modified'); ?></dt>
		<dd>
			<?= h($aclUser->modified); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('Edit Acl User'), ['action' => 'edit', $aclUser->id]); ?> </li>
		<li><?= $this->Form->postLink(__('Delete Acl User'), ['action' => 'delete', $aclUser->id], [], __('Are you sure you want to delete # %s?', $aclUser->id)); ?> </li>
		<li><?= $this->Html->link(__('List Acl Users'), ['action' => 'index']); ?> </li>
		<li><?= $this->Html->link(__('New Acl User'), ['action' => 'add']); ?> </li>
	</ul>
</div>
