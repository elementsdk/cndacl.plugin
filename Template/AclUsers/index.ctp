<div class="aclUsers index">
	<h2><?= __('Acl Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?= $this->Paginator->sort('id'); ?></th>
		<th><?= $this->Paginator->sort('acl_group_id'); ?></th>
		<th><?= $this->Paginator->sort('username'); ?></th>
		<th><?= $this->Paginator->sort('password'); ?></th>
		<th><?= $this->Paginator->sort('created'); ?></th>
		<th><?= $this->Paginator->sort('modified'); ?></th>
		<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($aclUsers as $aclUser): ?>
	<tr>
		<td><?= h($aclUser->id); ?>&nbsp;</td>
		<td><?= h($aclUser->acl_group_id); ?>&nbsp;</td>
		<td><?= h($aclUser->username); ?>&nbsp;</td>
		<td><?= h($aclUser->password); ?>&nbsp;</td>
		<td><?= h($aclUser->created); ?>&nbsp;</td>
		<td><?= h($aclUser->modified); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), ['action' => 'view', $aclUser->id]); ?>
			<?= $this->Html->link(__('Edit'), ['action' => 'edit', $aclUser->id]); ?>
			<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $aclUser->id], [], __('Are you sure you want to delete # %s?', $aclUser->id)); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<p><?= $this->Paginator->counter(); ?></p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'));
		echo $this->Paginator->numbers();
		echo $this->Paginator->next(__('next') . ' >');
	?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Acl User'), ['action' => 'add']); ?></li>
	</ul>
</div>
