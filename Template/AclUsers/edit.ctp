<div class="aclUsers form">
<?= $this->Form->create($aclUser); ?>
	<fieldset>
		<legend><?= __('Edit Acl User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('acl_group_id');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
	?>
	</fieldset>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $aclUser->id], [], __('Are you sure you want to delete # %s?', $aclUser->id)); ?></li>
		<li><?= $this->Html->link(__('List Acl Users'), ['action' => 'index']); ?></li>
	</ul>
</div>
