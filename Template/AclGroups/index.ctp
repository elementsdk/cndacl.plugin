<div class="aclGroups index">
	<h2><?= __('Acl Groups'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?= $this->Paginator->sort('id'); ?></th>
		<th><?= $this->Paginator->sort('name'); ?></th>
		<th><?= $this->Paginator->sort('created'); ?></th>
		<th><?= $this->Paginator->sort('modified'); ?></th>
		<th class="actions"><?= __('Actions'); ?></th>
	</tr>
	<?php foreach ($aclGroups as $aclGroup): ?>
	<tr>
		<td><?= h($aclGroup->id); ?>&nbsp;</td>
		<td><?= h($aclGroup->name); ?>&nbsp;</td>
		<td><?= h($aclGroup->created); ?>&nbsp;</td>
		<td><?= h($aclGroup->modified); ?>&nbsp;</td>
		<td class="actions">
			<?= $this->Html->link(__('View'), ['action' => 'view', $aclGroup->id]); ?>
			<?= $this->Html->link(__('Edit'), ['action' => 'edit', $aclGroup->id]); ?>
			<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $aclGroup->id], [], __('Are you sure you want to delete # %s?', $aclGroup->id)); ?>
		</td>
	</tr>
	<?php endforeach; ?>
	</table>
	<p><?= $this->Paginator->counter(); ?></p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'));
		echo $this->Paginator->numbers();
		echo $this->Paginator->next(__('next') . ' >');
	?>
	</div>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('New Acl Group'), ['action' => 'add']); ?></li>
	</ul>
</div>
