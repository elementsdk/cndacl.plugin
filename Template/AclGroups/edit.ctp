<div class="aclGroups form">
<?= $this->Form->create($aclGroup); ?>
	<fieldset>
		<legend><?= __('Edit Acl Group'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $aclGroup->id], [], __('Are you sure you want to delete # %s?', $aclGroup->id)); ?></li>
		<li><?= $this->Html->link(__('List Acl Groups'), ['action' => 'index']); ?></li>
	</ul>
</div>
