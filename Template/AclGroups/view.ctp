<div class="aclGroups view">
	<h2><?= __('Acl Group'); ?></h2>
	<dl>
		<dt><?= __('Id'); ?></dt>
		<dd>
			<?= h($aclGroup->id); ?>
			&nbsp;
		</dd>
		<dt><?= __('Name'); ?></dt>
		<dd>
			<?= h($aclGroup->name); ?>
			&nbsp;
		</dd>
		<dt><?= __('Created'); ?></dt>
		<dd>
			<?= h($aclGroup->created); ?>
			&nbsp;
		</dd>
		<dt><?= __('Modified'); ?></dt>
		<dd>
			<?= h($aclGroup->modified); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('Edit Acl Group'), ['action' => 'edit', $aclGroup->id]); ?> </li>
		<li><?= $this->Form->postLink(__('Delete Acl Group'), ['action' => 'delete', $aclGroup->id], [], __('Are you sure you want to delete # %s?', $aclGroup->id)); ?> </li>
		<li><?= $this->Html->link(__('List Acl Groups'), ['action' => 'index']); ?> </li>
		<li><?= $this->Html->link(__('New Acl Group'), ['action' => 'add']); ?> </li>
	</ul>
</div>
