<div class="aclGroups form">
<?= $this->Form->create($aclGroup); ?>
	<fieldset>
		<legend><?= __('Add Acl Group'); ?></legend>
	<?php
		echo $this->Form->input('name');
	?>
	</fieldset>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('List Acl Groups'), ['action' => 'index']); ?></li>
	</ul>
</div>
