<?php
namespace CndAcl\Console\Command;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use CndAcl\Lib\AclReflector;
use CndAcl\Lib\AclReflectorComponent;
use CndAcl\Test\Fixture\AclUserFixture;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Schema\Table;

/**
 * Setup shell command.
 */
class SetupShell extends Shell {

	/**
	 * main() method.
	 *
	 * @return bool|int Success or error code.
	 */
	public function main() {

		$aclReflector = new AclReflector();
		$reflections  = $aclReflector->getAllActions();
		$acos         = TableRegistry::get('CndAcl.Acos');

		foreach ($reflections as $reflection) {

			$root              = $acos->node('controllers')->first();
			$plugin_check      = $acos->node('controllers/' . $reflection['namespace'])->find('all')->toArray();
			$plugin_parent_ent = array_pop($plugin_check);

			if (empty($plugin_check)) {
				$plugin_parent_ent = $acos->newEntity([
					'alias'     => $reflection['namespace'],
					'parent_id' => $root->id]);
				$acos->save($plugin_parent_ent);
			}

			foreach ($reflection['controllers'] as $controller) {

				$controller_check         = $acos->node('controllers/' . $reflection['namespace'] . '/' . $controller['controller'])->find('all')->toArray();
				$controller_parent_entity = array_pop($controller_check);

				if (empty($controller_check)) {
					$controller_parent_entity = $acos->newEntity([
						'alias'     => $controller['controller'],
						'parent_id' => $plugin_parent_ent->id]);
					$acos->save($controller_parent_entity);
				}

				foreach ($controller['actions'] as $action) {

					$action_check  = $acos->node('controllers/' . $reflection['namespace'] . '/' . $controller['controller'] . '/' . $action)->find('all')->toArray();
					$action_entity = array_pop($action_check);
					if (empty($action_check)) {
						$action_entity = $acos->newEntity([
							'alias'     => $action,
							'parent_id' => $controller_parent_entity->id]);
						$acos->save($action_entity);
					}

				}
			}
		}

		$acos->recover();

	}
}
