<?php

namespace CndAcl\Lib;

use Cake\Core\App;
use Cake\Core\Plugin;
use Cake\Utility\Folder;
use Cake\Utility\Inflector;
use Cake\Utility\String;
use CndAcl\Model\Table\AclGroupsTable;

class AclReflector {


	public function getAllActions() {

		$plugin_names = $this->get_all_plugins_paths();
		return $plugin_names;

	}


	public function get_all_plugins_paths() {

		$plugin_names = Plugin::loaded();
		$plugin_paths = array();
		foreach ($plugin_names as $plugin_name) {
			$plugin_paths[] = [
				'namespace'   => $plugin_name,
				'path'        => Plugin::path($plugin_name),
				'controllers' => $this->getControllersFor(Plugin::path($plugin_name), $plugin_name)];
		}

		return $plugin_paths;

	}


	public function getControllersFor($path, $plugin = null) {

		$controllers = array();
		$folder      = new Folder();
		$didCD       = $folder->cd($path . 'Controller');

		if (!empty($didCD)) {

			$files = $folder->findRecursive('.*Controller\.php');

			foreach ($files as $fileName) {

				$file                  = basename($fileName);
				$controller_class_name = substr($file, 0, strlen($file) - strlen('.php'));
				$controllers[]         = [
					'plugin'     => $plugin,
					'file'       => $fileName,
					'controller' => str_replace('Controller', '',$controller_class_name),
					'actions'    => $this->getControllerActions($controller_class_name, $plugin)

				];
			}
		}

		return $controllers;

	}


	/**
	 * Return the methods of a given class name.
	 * Depending on the $filter_base_methods parameter, it can return the parent methods.
	 *
	 * @param string $controller_class_name (eg: 'AcosController')
	 * @param boolean $filter_base_methods
	 */
	public function getControllerActions($controller_classname, $plugin = null) {

		$controller_classname = !empty($plugin) ? "$plugin.$controller_classname" : $controller_classname;
		$controller_classname = App::className($controller_classname, 'Controller');
		$methods              = get_class_methods($controller_classname);

		if (isset($methods) && !empty($methods)) {
			$baseMethods          = get_class_methods('Cake\Controller\Controller');
			$ctrl_cleaned_methods = array();
			foreach ($methods as $method) {
				if (!in_array($method, $baseMethods) && strpos($method, '_') !== 0) {
					$ctrl_cleaned_methods[] = $method;
				}
			}

			return $ctrl_cleaned_methods;

		} else {
			return array();
		}
	}
}